process.stdout.write("server INIT\n");
const express = require("express");
const fileUpload = require('express-fileupload');
const cors = require("cors");
const fs = require("fs");
const si = require("systeminformation");
const bodyParser = require("body-parser");
const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;


/*
  JWT
*/
const secret = "secret";

const jwt_opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("JWT"),
  secretOrKey: secret,
  ignoreExpiration: true
};

passport.use(
  new JwtStrategy(jwt_opts, (user, done) => {
    database.userByName(user.name, res => {
      if (res && user.name == res.name && user.password == res.password) {
        delete res.password;
        return done(null, res);
      } else {
        return done(null, false);
      }
    });
  })
);

// CONFIG AND STATUS OBJECTS
process.stdout.write("server LOAD config\n");
const config = JSON.parse(fs.readFileSync("./config.json", "utf8"));
if (!fs.existsSync(config.cache_folder)) {
  fs.mkdirSync(config.cache_folder);
}
if (process.env.DOMAIN) {
  config.domain = process.env.DOMAIN;
}
config.base_path = __dirname;
const status = {
  scanning_music: false,
  scanning_video: false
};

process.stdout.write("server DEF arrays\n");
const lists = {
  audio_quality: ["64", "96", "128", "192", "256", "320"],
  video_quality: ["480", "720", "1080"],
  user_role: ["admin", "moderator", "user"],
  lang: ["ENG", "GER", "RUS"],
  visibility: ["global", "instance", "owner", "hidden"]
};

process.stdout.write("server READ cpu cores\n");
si.cpu(function (data) {
  config.cpu_cores = data.cores;
});

exports.config = config;
exports.status = status;
exports.lists = lists;
exports.passport = passport;
exports.secret = secret;
exports.checkGuest = function checkGuest(req, res, next) {
  passport.authenticate('jwt', { session: false, }, async (error, user) => {
    if (user) {
      delete user.password;
      req.user = user;
      next();
    } else {
      database.system.allows((allows) => {
        if (allows.guests) {
          req.user = { _id: -1, name: "Guest" }
          next();
        }
        else {
          return res.status(403).end();
        }
      });
    }
  })(req, res, next);
}

require("./services/cleanup_utiles");
const database = require("./services/database");

const app = express();
const server = require("http").createServer(app);

app.use(bodyParser.json({ limit: "500mb" }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload({
  createParentPath: true
}));
const corsOptions = {
  origin: (origin, callback) => {
    if (!origin || config.allowed_domains.indexOf(origin.replace(/:\d*$/g, "")) !== -1) {
      callback(null, true);
    } else {
      database.system.domains((domains) => {
        if (domains.indexOf(origin.replace(/:\d*$/g, "")) !== -1) {
          callback(null, true);
        } else {
          callback("Origin not allowed: " + origin);
        }
      })

    }
  },
  methods: "GET,POST,PUT,DELETE",
  allowedHeaders: "*",
  credentials: true
};

app.use(cors(corsOptions));

// ROUTERS
process.stdout.write("server LOAD routers\n");
const router = require("./router");
process.stdout.write("server USE routers\n");
app.use("/api/albums", router.albumRouter);
app.use("/api/artists", router.artistRouter);
app.use("/api/boxes", router.boxRouter);
app.use("/api/info", router.infoRouter);
app.use("/api/radios", router.radioRouter);
app.use("/api/scan", router.scanRouter);
app.use("/api/search", router.searchRouter);
app.use("/api/settings", router.settingsRouter);
app.use("/api/status", router.statusRouter);
app.use("/api/system", router.systemRouter);
app.use("/api/tracks", router.trackRouter);
app.use("/api/user", router.loginRouter);
app.use("/api/user", router.userRouter);
app.use("/api/videos", router.videoRouter);
// app.use("/well-known", router.wellknownRouter);
// app.use("/users", router.activitypubRouter);

process.stdout.write("server START listening on Port " + config.port + "\n");
server.listen(config.port, function () {
  process.stdout.write("Server startet on Port " + config.port + "...\n");
  process.stdout.write("Music Folder: " + config.music_folder + "\n");
  process.stdout.write("Video Folder: " + config.video_folder + "\n");
  process.stdout.write("Cache Folder: " + config.cache_folder + "\n");
  process.stdout.write("Upload Folder: " + config.upload_folder + "\n");
  process.stdout.write("Domain: " + config.domain + "\n");
});