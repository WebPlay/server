# WebPlay Server
[![Build Status](https://drone.anufrij.de/api/badges/WebPlay/server/status.svg)](https://drone.anufrij.de/WebPlay/server)

WebPlay Server provides REST-API for your media files. In then next step, you can use [WebPlay Client](/WebPlay/client) to access your content.

## How to install your onw instance
[Documentation](/WebPlay/docker#requirements)

## Support
Join our Matrix room: <a href="https://matrix.to/#/#WebPlay:matrix.anufrij.de">#WebPlay:matrix.anufrij.de</a>

## Donate
<a href="https://www.paypal.me/ArtemAnufrij">PayPal</a> | <a href="https://liberapay.com/Artem/donate">LiberaPay</a> | <a href="https://www.patreon.com/ArtemAnufrij">Patreon</a>