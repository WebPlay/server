console.log("router/system INIT");
const express = require("express");
const router = new express.Router();
const database = require("../services/database");
const redis = require("../services/redis")
const jwt = require("jsonwebtoken");

const server = require("../server");
const secret = server.secret;
const passport = server.passport;
const config = server.config


router
  .route("")
  .get((req, res) => {
    database.system.allows((result) => {
      res.json(result).end();
    })
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") > -1) {
      database.system.setAllows(req.body, () => {
        res.status(200).end();
      })
    } else {
      res.status(403).end();
    }
  });

router
  .route("/domains")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") > -1) {
      let domains = {
        const: config.allowed_domains,
        dynamic: []
      }
      database.system.domains((result) => {
        result.forEach(domain => {
          domains.dynamic.push(domain);
        });
        res.json(domains).end();
      });
    } else {
      res.status(403).end();
    }
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") > -1) {
      database.system.setDomains(req.body, () => {
        res.status(200).end();
      });
    }
  })

router
  .route("/setup")
  .get((req, res) => {
    database.users.collection(users => {
      if (users && users.length > 0) {
        res.status(204).end();
      } else {
        res.status(200).end();
      }
    });
  })
  .post((req, res) => {
    process.stdout.write("add admin user\n");
    database.users.collection(users => {
      if (users && users.length > 0) {
        res.status(403).end();
      } else {
        let newUser = {
          name: req.body.username,
          password: req.body.password,
          roles: ["admin"]
        };
        database.addUser(newUser, result => {
          var token = jwt.sign(result, secret);
          result.token = "JWT " + token;
          result.history = [];
          res.json(result).end();
        });
      }
    });
  });

router
  .route("/reset/redis")
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") > -1) {
      redis.flushAll();
    }
  });

module.exports = router;
