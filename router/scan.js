console.log("router/scan INIT");
var express = require("express");
var file_scanner = require("../services/files_scanner");
var cover_excluder = require("../services/cover_excluder");

console.log("router/scan LOAD server settings");
var server = require("../server");
var status = server.status;
var config = server.config;
var passport = server.passport;

var router = new express.Router();

console.log("router/scan SET routers");
router
  .route("/music")
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (!status.scanning_music) {
      status.scanning_music = true;
      process.stdout.write("music scann started…\n");
      res.send("music scann started…");
      file_scanner.scann_local_music_files(config.music_folder);
      setTimeout(() => {
        file_scanner.scann_local_music_files(config.upload_folder);
      }, 1000);
    } else {
      res.send("please wait. server is scanning for music files…");
    }
  });

router
  .route("/video")
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (!status.scanning_video) {
      status.scanning_video = true;
      process.stdout.write("video scann started…\n");
      res.send("video scann started…");
      file_scanner.scann_local_video_files(config.video_folder);
      setTimeout(() => {
        file_scanner.scann_local_video_files(config.upload_folder);
      }, 1000);
    } else {
      res.send("please wait. server is scanning for video files…");
    }
  });

console.log("router/scan EXPORT routers");
module.exports = router;
