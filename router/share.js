console.log("router/share INIT");
var express = require("express");
var router = new express.Router();
const database = require("../services/database");

const server = require("../server");
var passport = server.passport;

router.route("/")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.includes("admin")) {
      database.share.collection(result => {
        res.json(result).status(200).end();
      })
    } else {
      res.status(403).end();
    }
  });
router.route("/:id")
  .get((req, res) => {
    database.share.byId(req.params.id, result => {
      res.json(result).status(200).end();
    });
  });

module.exports = router;
