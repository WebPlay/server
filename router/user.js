console.log("router/user INIT");
var express = require("express");
var database = require("../services/database");
var bcrypt = require("bcryptjs");
var router = new express.Router();

var server = require("../server");
var passport = server.passport;

router
  .route("")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.users.collection(result => {
      res.json(result).end();
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("add user\n");
    if (
      req.user.roles.includes("admin") ||
      req.user.roles.includes("moderator")
    ) {
      let newUser = {
        name: req.body.name,
        password: req.body.password
      };
      database.addUser(newUser, () => {
        res.end();
      });
    } else {
      res.status(401).end();
    }
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("delete user\n");
    if (req.user.roles.includes("admin") || req.user._id == req.query.id) {
      database.userById(req.query.id, (user) => {
        if (user.roles.includes("admin")) {
          res.status(403).end();
        } else {
          database.deleteUser(req.query.id, () => {
            database.users.collection(result => {
              res.json(result).end();
            });
          });
        }
      });

    } else {
      res.status(401).end();
    }
  })
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("update user\n");
    if (req.user.roles.includes("admin")) {
      if (req.body.newPassword) {
        database.updateUserPassword(req.body.name, req.body.newPassword);
      } else {
        database.updateUserRole(req.body, () => {
          res.status(202).end();
        });
      }
      res.end();
    } else {
      res.status(401).end();
    }
  });

router
  .route("/:name/exists")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (
      req.user.roles.includes("admin") ||
      req.user.roles.includes("moderator")
    ) {
      database.userByName(req.params.name, user => {
        res.json({ exists: user != null }).end();
      });
    } else {
      res.status(401).end();
    }
  });

router
  .route("/update")
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (!req.user) {
      return res.status(401).end();
    }
    if (!req.body.oldPassword) {
      database.updateUserConfig(req.user, req.body);
      process.stdout.write("config changed\n");
    }

    if (req.body.oldPassword && req.user.password) {
      bcrypt.compare(req.body.oldPassword, req.user.password, function (err, isMatch) {
        if (err) throw err;
        if (isMatch) {
          database.updateUserPassword(req.user.name, req.body.newPassword);
          process.stdout.write("password changed\n");
          res.status(202);
        } else {
          process.stdout.write("no match\n");
          res.status(422);
        }
        res.end();
      });
    } else {
      res.end();
    }
  });

router
  .route("/favourites")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("router/user GET favourites\n");
    database.user.favourites(id, result => {
      res.json(result).end();
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    let item = req.body;
    item.userId = req.user._id;
    process.stdout.write("router/user POST favourites\n");
    database.users.insertFavourite(item, () => {
      res.status(200).end();
    })
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    let item = {};
    item.itemId = req.query.itemId;
    item.userId = req.user._id;
    process.stdout.write("router/user DELETE favourites " + req.query.itemId + "\n");
    database.users.deleteFavourite(item, () => {
      res.status(200).end();
    });
  });

router
  .route("/history")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.historyList(req.user._id, result => {
      res.json(result).end();
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    let item = req.body;
    item.userId = req.user._id;
    database.updateHistory(item, () => {
      database.historyList(req.user._id, result => {
        res.json(result).end();
      });
    });
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.clearHistory(req.user._id, () => {
      process.stdout.write("history cleared for '" + req.user.name + "'\n");
      res.status(200).end();
    });
  });

router
  .route("/settings")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    req.user.player.repeat = req.body.repeat;
    req.user.player.shuffle = req.body.shuffle;
    database.updateUserSettings(req.user, () => {
      res.status(202).end();
    });
  });

module.exports = router;
