console.log("router/album INIT");
const express = require("express");
const database = require("../services/database");

const server = require("../server");
const passport = server.passport;
const checkGuest = server.checkGuest;
const resize_image_for_album = require("../services/cover/resizer").resize_image_for_album

const router = new express.Router();

router.route("/favourites")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.albums.favourites(req.user._id, result => {
      res.json(result).end();
    });
  })

router.route("/page/:page")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/album GET albums page " + req.params.page + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.albums.collection(req.params.page, filter, result => {
      process.stdout.write("router/album GET albums page " + req.params.page + " DB result\n");
      res.json(result);
    });
  });

router.route("/newest/:count")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/album GET newest " + req.params.count + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.albums.newest(parseInt(req.params.count), filter, result => {
      process.stdout.write("router/album GET newest " + req.params.count + " DB result\n");
      res.json(result);
    });
  });

router.route("/:id")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/album GET album by id " + req.params.id + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.albums.byId(req.params.id, filter, result => {
      process.stdout.write("router/album GET album by id " + req.params.id + " DB result\n");
      res.json(result);
    });
  })
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") >= 0 || req.user.roles.indexOf("moderator") >= 0) {
      database.albums.update(req.body);
    }
    res.end();
  });

router.route("/filter/:term")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("router/album GET filter by term " + req.params.term + "\n");
    database.albums.filter(req.params.term, result => {
      process.stdout.write("router/album GET filter by term " + req.params.term + " DB result\n");
      res.json(result);
    });
  })

router.route("/:id/cover")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.files) {
      if (req.files.file) {
        resize_image_for_album(req.files.file.data, (result) => {
          database.albums.updateCovers({ _id: req.params.id }, result);
          res.json(result);
        });
      }
    }

  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.albums.updateCovers({ _id: req.params.id }, {});
    res.end();
  });

router.route("/:id/move")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    let source = {};
    let target = {};
    database.albums.byId(req.body.source, undefined, (result) => {
      if (result != undefined && (req.user.roles.indexOf("admin") > -1) || result.owner_id == req.user._id) {
        source = result;
        database.albums.byId(req.body.target, undefined, (result) => {
          if (result && (req.user.roles.indexOf("admin") > -1 || result.owner_id == req.user._id)) {
            target = result;
            for (let i = 0; i < source.tracks.length; i++) {
              let track = source.tracks[i]
              track.album_id = target._id
              database.tracks.moveTo(track);
            }
            database.albums.delete(source, () => {
              res.status(200).end();
            });

          } else {
            res.status(403).end();
          }
        });
      } else {
        res.status(403).end();
      }
    });
  });

module.exports = router;