console.log("router/status INIT");
var express = require("express");
var router = new express.Router();

var server = require("../server");
var status = server.status;

var tag_excluder = require("../services/tag_excluder");

router.route("").get((req, res) => {
  status.scann_buffer = tag_excluder.get_buffer_size();
  status.parsing_music_data = tag_excluder.parsing_music_data();
  status.parsing_video_data = tag_excluder.parsing_video_data();
  status.parsing_cover = tag_excluder.parsing_cover();
  res.json(status);
});

module.exports = router;
