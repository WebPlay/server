console.log("router/radio INIT");
const express = require("express");
console.log("router/radio REQUIRE END");

const database = require("../services/database");

const server = require("../server");
const passport = server.passport;
const checkGuest = server.checkGuest;
const resize_image_for_radio = require("../services/cover/resizer").resize_image_for_radio;

console.log("router/radio CREATE instances");

var router = new express.Router();

console.log("router/radio READY");
router
  .route("")
  .get(checkGuest, (req, res) => {
    database.radios.collection(result => {
      res.json(result);
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("add radio\n");
    if (
      req.body.name == "" ||
      !req.body.url ||
      !req.body.url.startsWith("http")
    ) {
      return res.end();
    }
    let newRadio = {
      name: req.body.name,
      url: req.body.url
    };
    database.radios.add(newRadio, () => {
      res.end();
    });
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("delete radio\n");
    database.radios.delete(req.query.id, () => {
      database.radios.collection(result => {
        res.json(result);
      });
    });
  });

router
  .route("/:id/cover")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("update radio cover\n");
    if (req.files) {
      database.radios.byId(req.params.id, radio => {
        if (radio) {
          if (req.files.file) {
            resize_image_for_radio(req.files.file.data, (result) => {
              radio.cover32 = result.cover32;
              radio.cover64 = result.cover64;
              radio.cover128 = result.cover128;
              database.radios.update(radio);
              res.json(radio).end();
            });
          }
        }
      });
    }
  });

module.exports = router;
