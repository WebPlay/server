console.log("router/box INIT");
const express = require("express");
const database = require("../services/database");

const server = require("../server");
const passport = server.passport;
const checkGuest = server.checkGuest;
const resize_image_for_box = require("../services/cover/resizer").resize_image_for_box

const router = new express.Router();

router.route("/favourites")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.boxes.favourites(req.user._id, result => {
      res.json(result).end();
    });
  })

router
  .route("/page/:page")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/box GET boxes page " + req.params.page + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.boxes.collection(req.params.page, filter, result => {
      process.stdout.write("router/box GET boxes page " + req.params.page + " DB result\n");
      res.json(result);
    });
  });

router
  .route("/newest/:count")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/box GET newest " + req.params.count + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.boxes.newest(parseInt(req.params.count), filter, result => {
      process.stdout.write("router/box GET newest " + req.params.count + " DB result\n");
      res.json(result);
    });
  });

router
  .route("/:id")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/box GET by id " + req.params.id + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.boxes.byId(req.params.id, filter, result => {
      process.stdout.write("router/box GET by id " + req.params.id + " DB result\n");
      res.json(result).end();
    });
  }).put(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.roles.indexOf("admin") >= 0 || req.user.roles.indexOf("moderator") >= 0) {
      database.boxes.update(req.body);
    }
    res.end();
  });

router.route("/filter/:term")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("router/boxes GET filter by term " + req.params.term + "\n");
    database.boxes.filter(req.params.term, result => {
      process.stdout.write("router/boxes GET filter by term " + req.params.term + " DB result\n");
      res.json(result);
    });
  })

router.route("/:id/cover")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.files) {
      if (req.files.file) {
        resize_image_for_box(req.files.file.data, (result) => {
          database.boxes.updateCovers({ _id: req.params.id }, result);
          res.json(result);
        });
      }
    }
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.boxes.updateCovers({ _id: req.params.id }, {});
    res.end();
  });

router.route("/:id/move")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    let source = {};
    let target = {};
    database.boxes.byId(req.body.source, undefined, (result) => {
      if (result && (req.user.roles.indexOf("admin") > -1 || result.owner_id == req.user._id)) {
        source = result;
        database.boxes.byId(req.body.target, undefined, (result) => {
          if (result && (req.user.roles.indexOf("admin") > -1 || result.owner_id == req.user._id)) {
            target = result;
            for (let i = 0; i < source.videos.length; i++) {
              let video = source.videos[i]
              video.box_id = target._id
              database.videos.moveTo(video);
            }
            database.boxes.delete(source, () => {
              res.status(200).end();
            });
          } else {
            res.status(403).end();
          }
        });
      } else {
        res.status(403).end();
      }
    });
  });
module.exports = router;
