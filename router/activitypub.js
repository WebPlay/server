const express = require("express");
const router = new express.Router();

const database = require("../services/database");

const server = require("../server");
const config = server.config;


router.route("/:user")
  .get((req, res) => {
    database.userByName(req.params.user.toLowerCase(), user => {
      if (user == undefined) {
        return req.status(404).end();
      }

      let id = "https://" + config.domain + "/users/" + user.name;
      let inbox = "https://" + config.domain + "/users/" + user.name + "/inbox";
      let outbox = "https://" + config.domain + "/users/" + user.name + "/outbox";
      let followers = "https://" + config.domain + "/users/" + user.name + "/followers";
      let following = "https://" + config.domain + "/users/" + user.name + "/followinig";
      let liked = "https://" + config.domain + "/users/" + user.name + "/liked";

      let response = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Person",
        "id": id,
        "name": "Artem",
        "preferredUsername": user.name,
        "summary": "cajon player and e-bass junkie",
        "inbox": inbox,
        "outbox": outbox,
        "followers": followers,
        "following": following,
        "liked": liked
      }
      res.json(response).end();
    });
  });

router.route("/:user/inbox")
  .get((req, res) => {
    console.log(req);
    res.end();
  })
  .post((req, res) => {
    console.log(req);
    res.end();
  });

router.route("/:user/outbox")
  .get((req, res) => {
  })
  .post((req, res) => {
    console.log(req);
    res.end();
  });

router.route("/:user/followers")
  .get((req, res) => {
    console.log(req);
    res.end();
  })
  .post((req, res) => {
    console.log(req);
    res.end();
  });
router.route("/:user/following")
  .get((req, res) => {
    console.log(req);
    res.end();
  })
  .post((req, res) => {
    console.log(req);
    res.end();
  });

router.route("/:user/liked")
  .get((req, res) => {
    console.log(req);
    res.end();
  })
  .post((req, res) => {
    console.log(req);
    res.end();
  });

module.exports = router;
