console.log("router/search INIT");
var express = require("express");
var server = require("../server");
var database = require("../services/database");
var passport = server.passport;

var router = new express.Router();

router
  .route("/:term")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.search(req.params.term.toLowerCase(), result => {
      res.json(result).end();
    });
  });

module.exports = router;
