const express = require("express");
const router = new express.Router();

const server = require("../server");
const config = server.config;
const database = require("../services/database");


router.route("/webfinger")
  .get((req, res) => {
    if (!req.query.resource) {
      return res.status(400).end();
    }

    let resource = req.query.resource.toLowerCase();
    let name = resource.replace("acct:", "").replace("@" + config.domain, "").toLowerCase();

    if (name.length == 0) {
      return res.status(400).end();
    }

    database.userByName(name, (user) => {
      if (user == undefined) {
        return res.status(404).end();
      }
      let response = {
        "subject": resource,
        "aliases": [
          "https://" + config.domain + "/#/users/" + user.name
        ],
        "links": [
          {
            "rel": "http://webfinger.net/rel/profile-page",
            "type": "text/html",
            "href": "https://" + config.domain + "/#/users/" + user.name
          },
          {
            "rel": "self",
            "type": "application/activity+json",
            "href": "https://" + config.domain + "/users/" + user.name
          }
        ]

      }
      res.json(response).end();
    });
  });

module.exports = router;