console.log("router/artist INIT");
const express = require("express");
console.log("router/artist REQUIRE database");
const database = require("../services/database");

const server = require("../server");
const passport = server.passport;
const checkGuest = server.checkGuest;
const resize_image_for_artist = require("../services/cover/resizer").resize_image_for_artist

const router = new express.Router();

router.route("/favourites")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.artists.favourites(req.user._id, result => {
      res.json(result).end();
    });
  })

router
  .route("/page/:page")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/artist GET artists page " + req.params.page + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.artists.collection(req.params.page, filter, result => {
      process.stdout.write("router/artist GET artists page " + req.params.page + " DB result\n");
      res.json(result);
    });
  });

router
  .route("/:id")
  .get(checkGuest, (req, res) => {
    process.stdout.write("router/artist GET by id " + req.params.id + "\n");
    let filter = undefined;
    if (req.user._id == -1) {
      filter = ['global'];
    }
    database.artists.byId(req.params.id, filter, result => {
      process.stdout.write("router/artist GET by id " + req.params.id + " DB result\n");
      res.json(result);
    });
  });

router.route("/filter/:term")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("router/artist GET filter by term " + req.params.term + "\n");
    database.artists.filter(req.params.term, result => {
      process.stdout.write("router/artist GET filter by term " + req.params.term + " DB result\n");
      res.json(result);
    });
  })

router.route("/:id/cover")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.files) {
      if (req.files.file) {
        resize_image_for_artist(req.files.file.data, (result) => {
          database.artists.updateCovers({ _id: req.params.id }, result);
          res.json(result);
        });
      }
    }
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    database.artists.updateCovers({ _id: req.params.id }, {});
    res.end();
  });

router.route("/:id/move")
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    let source = {};
    let target = {};
    database.artists.byId(req.body.source, undefined, (result) => {
      if (result && req.user.roles.indexOf("admin") > -1) {
        source = result;
        database.artists.byId(req.body.target, undefined, (result) => {
          if (result && req.user.roles.indexOf("admin") > -1) {
            target = result;
            for (let i = 0; i < source.albums.length; i++) {
              let album = source.albums[i];
              album.artist_id = target._id;
              album.artist_name = target.name;
              database.albums.moveTo(album);
            }
            database.artists.delete(source, () => {
              res.status(200).end();
            });

          } else {
            res.status(403).end();
          }
        });
      } else {
        res.status(403).end();
      }
    });
  });
module.exports = router;