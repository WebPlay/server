console.log("router/info INIT");
var express = require("express");
var router = new express.Router();
var database = require("../services/database");

let info = {}
info.stats = {}

router
    .route("")
    .get((req, res) => {
        database.artist_count((q, c) => {
            info.stats.artists = c;
            database.album_count((q, c) => {
                info.stats.albums = c;
                database.track_count((q, c) => {
                    info.stats.tracks = c;
                    database.box_count((q, c) => {
                        info.stats.boxes = c;
                        database.video_count((q, c) => {
                            info.stats.videos = c;
                            database.users_count((q, c) => {
                                info.stats.users = c;
                                res.json(info);
                            });
                        });
                    });
                });
            });
        });
    });

module.exports = router;
