console.log("router/login INIT");
var express = require("express");
var router = new express.Router();
var bcrypt = require("bcryptjs");
var database = require("../services/database");
var jwt = require("jsonwebtoken");

var server = require("../server");
var passport = server.passport;
var secret = server.secret;

router
  .route("/login")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    let user = req.user;
    database.historyList(user._id, history => {
      user.history = history;
      database.users.favourites(user._id, favourites => {
        user.favourites = favourites;
        res.status(200).json(req.user);
      });
    });
  })
  .post((req, res) => {
    database.userByName(req.body.username, user => {
      if (!user) {
        process.stdout.write("no user in DB\n");
        return res.status(401).send({
          success: false,
          msg: "Authentication failed. Wrong user."
        });
      }

      bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
        if (err) throw err;
        if (isMatch) {
          var token = jwt.sign(user, secret);
          user.token = "JWT " + token;
          process.stdout.write("pass ok\n");
          database.updateUserAccess(req.body.username);
          database.historyList(user._id, history => {
            user.history = history;
            database.users.favourites(user._id, favourites => {
              user.favourites = favourites;
              res.status(200).json(user);
            });
          });
        } else {
          process.stdout.write("pass fail\n");
          res.status(401).send({
            success: false,
            msg: "Authentication failed. Wrong password."
          });
        }
      });
    });
  });

module.exports = router;
