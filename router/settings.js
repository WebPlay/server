console.log("router/settings INIT");
var express = require("express");
var database = require("../services/database");
var router = new express.Router();

var server = require("../server");

router.route("/").get((req, res) => {
  database.system.allows((result) => {
    console.log(result);
    res.json(result)
  });
});

router.route("/lists").get((req, res) => {
  let lists = {
    audio_quality: server.lists.audio_quality,
    video_lang: server.lists.lang,
    video_quality: server.lists.video_quality,
    visibility: server.lists.visibility,
    user_role: server.lists.user_role
  }
  res.json(lists);
});

module.exports = router;
