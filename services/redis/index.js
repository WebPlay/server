const { createClient } = require('redis');
const server = require("../../server");
const config = server.config;

const redisUrl = "redis://" + config.redis.host + ":" + config.redis.port

const client = createClient({
    url: redisUrl
});

client.on('error', (err) => console.log('Redis Client Error', err));

client.connect();
client.flushAll();

const expire = 57600; // 24h

exports.set = function (key, value) {
    if (value) {
        client.set(key, JSON.stringify(value));
        client.expire(key, expire);
    }
}

exports.get = function (key, callback) {
    process.stdout.write("services/redis get '" + key + "'\n");
    client.get(key).then(value => {
        callback(JSON.parse(value));
    });
    client.expire(key, expire);
}

exports.flushAll = function () {
    client.flushAll();
    process.stdout.write("services/redis flushAll()\n");
}