var fs = require("fs");
var mime = require("mime-types");
var notifier = require("./notifier");
var database = require("./database");

var server = require("../server");
var config = server.config;

/*
MUSIC
*/

exports.scann_local_music_files = async function (path) {
  remove_non_exists_tracks();
  scann_local_music_files(path);
};

async function remove_non_exists_tracks() {
  database.tracks.collection(tracks => {
    tracks.forEach(track => {
      let path = config.music_folder + track.path;
      if (!fs.existsSync(path)) {
        path = config.upload_folder + track.path;
        if (!fs.existsSync(path)) {
          database.tracks.delete(track, () => {
            notifier.emit("track_deleted", track._id);
            process.stdout.write("track deleted: " + path + "\n");
          });
        }
      }
    });
    process.stdout.write("check empty albums\n");
    database.albums.empty(albums => {
      albums.forEach(album => {
        database.albums.delete(album, () => {
          notifier.emit("album_deleted", album._id);
          process.stdout.write("album deleted: " + album.title + "\n");
        });
      });
      process.stdout.write("check empty artists\n");
      database.artists.empty(artists => {
        artists.forEach(artist => {
          database.artists.delete(artist, () => {
            notifier.emit("artist_deleted", artist._id);
            process.stdout.write("artist deleted: " + artist.name + "\n");
          });
        });
      });
    });
  });
}

async function scann_local_music_files(path) {
  if (!fs.existsSync(path)) {
    return;
  }
  fs.readdirSync(path).forEach(child => {
    if (!child.startsWith(".")) {
      var full_path = path + "/" + child;
      if (fs.lstatSync(full_path).isDirectory()) {
        //setTimeout(() => {
        scann_local_music_files(full_path);
        //}, 1000);
      } else {
        var mime_type = mime.lookup(full_path);
        if (
          mime_type &&
          mime_type.startsWith("audio/") &&
          mime_type.indexOf("x-mpegurl") == -1 &&
          mime_type.indexOf("x-scpls") == -1
        ) {
          let item = { path: full_path, mime: mime_type };
          notifier.emit("music_file_found", item);
        }
      }
    }
  });
}

/*
VIDEO
*/

exports.scann_local_video_files = async function (path) {
  remove_non_exists_videos();
  scann_local_video_files(path);
};

async function remove_non_exists_videos() {
  database.videos.collection(videos => {
    videos.forEach(video => {
      let path = config.video_folder + video.path;
      if (!fs.existsSync(path)) {
        path = config.upload_folder + video.path;
        if (!fs.existsSync(path)) {
          database.videos.delete(video, () => {
            notifier.emit("video_deleted", video._id);
            process.stdout.write("video deleted: " + path + "\n");
          });
        }
      }
    });
    process.stdout.write("check empty boxes\n");
    database.boxes.empty(boxes => {
      boxes.forEach(box => {
        database.boxes.delete(box, () => {
          notifier.emit("box_deleted", box._id);
          process.stdout.write("box deleted: " + box.title + "\n");
        });
      });
    });
  });
}

async function scann_local_video_files(path) {
  if (!fs.existsSync(path)) {
    return;
  }
  fs.readdirSync(path).forEach(child => {
    if (!child.startsWith(".")) {
      var full_path = path + "/" + child;
      if (fs.lstatSync(full_path).isDirectory()) {
        //setTimeout(() => {
        scann_local_video_files(full_path);
        //}, 1000);
      } else {
        var mime_type = mime.lookup(full_path);
        if (mime_type && mime_type.startsWith("video/")) {
          let item = { path: full_path, mime: mime_type };
          notifier.emit("video_file_found", item);
        }
      }
    }
  });
}
