console.log("service/cover_excluder INIT");
console.log("service/cover_excluder REQUIRE fs");
const fs = require("fs");
console.log("service/cover_excluder REQUIRE path");
const path = require("path");
console.log("service/cover_excluder REQUIRE request");
const request = require("request");

console.log("service/cover_excluder INCLUDE notifier");
const notifier = require("./notifier");
console.log("service/cover_excluder INCLUDE database");
const database = require("./database");
console.log("service/cover_excluder INCLUDE music_brainz");
const music_brainz = require("./music_brainz");
console.log("service/cover_excluder INCLUDE the_movie_db");
const the_movie_db = require("./the_movie_db");
console.log("service/cover_excluder INCLUDE resizer")
const resizer = require("../services/cover/resizer")


console.log("service/cover_excluder LOAD server settings")
const server = require("../server");
const status = server.status;
const config = server.config;
const cache_folder = config.cache_folder + "/";


console.log("service/cover_excluder SET notifiers")
notifier.on("exclude_metadata_finished", () => {
  process.stdout.write("music scann finished…\n");
  checkMissingAlbumCovers();
  checkMissingArtistCovers();
  status.scanning_music = false;
});

notifier.on("exclude_pathdata_finished", () => {
  process.stdout.write("video scann finished…\n");
  checkMissingBoxCovers();
  status.scanning_video = false;
});

notifier.on("metadata_picture_excluded", album => {
  resizer.resize_image_for_album(album.picture[0].data, covers => {
    if (Object.keys(covers).length > 0) {
      process.stdout.write("cover from metadata…\n");
      database.albums.updateCovers(album, covers);
    } else {
      process.stdout.write("no cover from metadata…\n");
      music_brainz.find_album_cover(album);
    }
  });
});

notifier.on("no_metadata_picture_excluded", album => {
  music_brainz.find_album_cover(album);
});

notifier.on("found_music_brainz_album_cover", result => {
  process.stdout.write("MB album result: " + result.mb + "\n");

  let album_id = result.album._id.toString();
  let tmp_file = cache_folder + album_id;

  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    resizer.resize_image_for_album(tmp_file, covers => {
      if (Object.keys(covers).length > 0) {
        process.stdout.write("album cover from musicbrainz…\n");
        database.albums.updateCovers(result.album, covers);
      }
      fs.unlinkSync(tmp_file);
    });
  });

  request(result.mb).pipe(writer);
});

notifier.on("found_music_brainz_artist_cover", result => {
  process.stdout.write("MB artist result: " + result.mb + "\n");

  let artist_id = result.artist._id.toString();
  let tmp_file = cache_folder + artist_id;
  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    resizer.resize_image_for_artist(tmp_file, covers => {
      if (Object.keys(covers).length > 0) {
        process.stdout.write("artist cover from musicbrainz…\n");
        database.artists.updateCovers(result.artist, covers);
      }
      fs.unlinkSync(tmp_file);
    });
  });

  request(result.mb).pipe(writer);
});

notifier.on("found_movie_db_box_cover", box => {
  process.stdout.write("TMVD box result: " + box.tmdb + "\n");

  let box_id = box._id.toString();
  let tmp_file = cache_folder + box_id;
  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    resizer.resize_image_for_box(tmp_file, covers => {
      if (Object.keys(covers).length > 0) {
        process.stdout.write("box covers from tmvd…\n");
        database.boxes.updateCovers(box, covers);
      }
      fs.unlinkSync(tmp_file);
    });
  });

  request(box.tmdb).pipe(writer);
});

function checkMissingAlbumCovers() {
  process.stdout.write("started cover excluding for albums…\n");
  database.albums.collection(-1, null, albums => {
    albums.forEach(album => {
      database.albums.tracks(album._id, album_tracks => {
        if (!album_tracks || !album_tracks.tracks || album.covers) {
          return;
        }
        album.tracks = album_tracks.tracks;
        let cover_found = false;
        album.tracks.forEach(track => {
          if (cover_found) {
            return;
          }

          let file_path = config.music_folder + track.path;
          let directory = path.dirname(file_path);

          config.album_cover_files.forEach(file => {
            let img = directory + "/" + file;
            if (fs.existsSync(img)) {
              cover_found = true;
              resizer.resize_image_for_album(img, covers => {
                if (Object.keys(covers).length > 0) {
                  process.stdout.write("covers from file…\n");
                  database.albums.updateCovers(album, covers);
                }
              });
              return;
            }
          });
        });

        if (!cover_found) {
          export_from_id3(album);
        }
      });
    });
  });
}

function export_from_id3(album) {
  album.tracks.forEach(track => {
    track.parent = album;
  });
  notifier.emit("album_cover_request", album.tracks[0]);
}

function checkMissingArtistCovers() {
  process.stdout.write("started cover excluding for artists…\n");
  database.artists.collection(-1, undefined, artists => {
    artists.forEach(artist => {
      database.artists.tracks(artist._id.toString(), true, artist_full => {
        if (!artist_full || !artist_full.albums || artist.covers) {
          return;
        }
        let cover_found = false;
        artist_full.albums.forEach(album => {
          album.tracks.forEach(track => {
            if (cover_found) {
              return;
            }
            config.artist_cover_files.forEach(file => {
              if (cover_found) {
                return;
              }
              let file_path = config.music_folder + track.path;
              let directory = path.dirname(file_path);
              let img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                resizer.resize_image_for_artist(img, covers => {
                  if (Object.keys(covers).length > 0) {
                    process.stdout.write("artist covers from file…\n");
                    database.artists.updateCovers(artist, covers);
                  }
                });
                return;
              }

              directory = path.dirname(directory);
              img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                resizer.resize_image_for_artist(img, covers => {
                  if (Object.keys(covers).length > 0) {
                    process.stdout.write("artist covers from file…\n");
                    database.artists.updateCovers(artist, covers);
                  }
                });
                return;
              }

              directory = path.dirname(directory);
              img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                resizer.resize_image_for_artist(img, covers => {
                  if (Object.keys(covers).length > 0) {
                    process.stdout.write("artist covers from file…\n");
                    database.artists.updateCovers(artist, covers);
                  }
                });
                return;
              }
            });
          });
        });

        if (!cover_found) {
          music_brainz.find_artist_cover(artist_full);
        }
      });
    });
  });
}

function checkMissingBoxCovers() {
  process.stdout.write("started cover excluding for boxes…\n");
  database.boxes.collection(-1, undefined, boxes => {
    boxes.forEach(box => {
      database.boxes.videos(box._id.toString(), true, box_full => {
        if (box_full.covers) {
          return;
        }
        let cover_found = false;
        let directory = config.video_folder + box_full.path;
        config.box_cover_files.forEach(file => {
          if (cover_found) {
            return;
          }
          let img = directory + "/" + file;
          if (fs.existsSync(img)) {
            cover_found = true;
            resizer.resize_image_for_box(img, covers => {
              if (Object.keys(covers).length > 0) {
                process.stdout.write("box covers from file…\n");
                database.boxes.updateCovers(box_full, covers);
              }
            });
            return;
          }
        });

        if (!cover_found) {
          the_movie_db.find_box_cover(box);
        }
      });
    });
  });
}