const { MongoClient } = require('mongodb');
const server = require("../../server");
const config = server.config;

var dbo;
const url = "mongodb://" + config.database.host + ":" + config.database.port + "/";
const database = config.database.name;

exports.connect = async function () {
    if (dbo) {
        console.log("DB CONNECTED")
        return dbo;
    }
    else {
        try {
            console.log("DB CONNECTING:" + config.database.host + ":" + config.database.port)
            const client = await MongoClient.connect(url);
            dbo = client.db(database);
            return dbo;
        } catch (error) {
            console.error(`MongoDB connection failed with > ${error}`);
            throw error;
        }
    }
}