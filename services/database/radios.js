const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
});

exports.collection = function (callback) {
  dbo
    .collection("radios")
    .find({})
    .sort({ name: 1 })
    .toArray((err, result) => {
      result.forEach(item => {
        item.type = "radio";
      });
      callback(result);
    });
};

exports.byId = function (id, callback) {
  dbo
    .collection("radios")
    .findOne({ _id: ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.add = function (radio, callback) {
  dbo.collection("radios").updateOne(
    { url: radio.url },
    {
      $set: {
        name: radio.name,
        url: radio.url
      }
    },
    { upsert: true },
    err => {
      if (err) throw err;
      dbo.collection("radios").findOne({ url: radio.url }, (err, result) => {
        if (err) throw err;
        callback(result);
      });
    }
  );
};

exports.delete = function (id, callback) {
  dbo
    .collection("radios")
    .deleteOne({ _id: ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.update = function (radio, callback) {
  dbo.collection("radios").updateOne(
    { _id: radio._id },
    {
      $set: {
        name: radio.name,
        url: radio.url,
        cover32: radio.cover32,
        cover64: radio.cover64,
        cover128: radio.cover128
      }
    },
    { upsert: false },
    err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    }
  );
};