const redis = require("../redis")
const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
  dbo.collection("artists").createIndex({ name: 1 });
  // TEMPORARY
  dbo.collection("artists").updateMany({}, { $unset: { cover32: 1, cover64: 1, cover128: 1, cover256: 1, cover512: 1 } })
});

let artist_lookup_album = {
  $lookup: {
    from: "albums",
    localField: "_id",
    foreignField: "artist_id",
    as: "albums"
  }
}
let artist_lookup_tracks = {
  $lookup: {
    from: "tracks",
    localField: "albums._id",
    foreignField: "album_id",
    as: "albums.tracks"
  }
}
let artists_project = {
  $project: {
    "covers.cover64": false,
    "covers.cover128": false,
    "covers.cover512": false,
    "covers.cover1024": false,
  }
}
let artist_project = {
  $project: {
    "albums.tracks.path": false,
    "albums.tracks.bitrate": false,
    "albums.tracks.album_id": false,
    "albums.tracks.mime": false,
    "albums.artist_id": false,
    "albums.covers.cover256": false,
    "albums.covers.cover512": false
  }
}
exports.collection = function (page, filter, callback) {
  process.stdout.write("services/db_manager ARTISTS Collection: " + page + "\n");
  let redis_key = "artistsCollection_" + (filter || '') + '_' + page;

  redis.get(redis_key, (value) => {
    if (value) {
      process.stdout.write("services/db_manager ARTISTS Collection REDIS: " + page + "\n");
      callback(value);
    } else {

      let aggregate = []
      if (filter) {
        aggregate.push(
          artist_lookup_album, {
          $match: { "albums.visibility": { $in: filter } }
        });
      }
      aggregate.push(
        artists_project,
        { $sort: { name: 1 } }
      )
      if (page > -1) {
        let pageSize = 12;
        let skip = (page - 1) * pageSize;
        aggregate.push(
          { $skip: skip },
          { $limit: pageSize });
      }

      dbo
        .collection("artists")
        .aggregate(aggregate, { allowDiskUse: true })
        .toArray((err, result) => {
          if (err) throw err;
          if (result) {
            result.forEach(item => {
              item.type = "artist";
              item.albums = [];
              item.tracks = [];
            });
          }
          process.stdout.write("services/db_manager ARTISTS Collection MONGO: " + page + "\n");
          callback(result);
          redis.set(redis_key, result);
        });
    }
  });
};

exports.favourites = function (id, callback) {
  dbo.collection("favourites")
    .find({ userId: id, type: "artist" })
    .toArray((err, favourites) => {
      if (err) throw err;
      let aggregate = [
        { $match: { _id: { $in: favourites.map(m => m.itemId) } } },
        artists_project,
      ]
      dbo.collection("artists")
        .aggregate(aggregate)
        .toArray((err, result) => {
          result.forEach(item => {
            item.type = "artist";
            item.albums = [];
            item.tracks = [];
          });
          callback(result);
        });
    })
};

exports.byId = function (id, filter, callback) {
  process.stdout.write("services/db_manager ARTIST by id: " + id + "\n");
  let redis_key = "artistId_" + (filter || '') + '_' + id;

  redis.get(redis_key, (value) => {
    if (value) {
      process.stdout.write("services/db_manager ARTIST by id REDIS: " + id + "\n");
      callback(value);
    } else {

      let aggregate = [
        artist_lookup_album,
        {
          $unwind: { path: "$albums" }
        },
        artist_lookup_tracks,
        artist_project,
        {
          $group: {
            _id: "$_id",
            name: { $first: "$name" },
            covers: { $first: "$covers" },
            albums: { $push: "$albums" }
          }
        },
        { $match: { _id: ObjectId(id) } },
      ];
      dbo
        .collection("artists")
        .aggregate(aggregate)
        .toArray((err, result) => {
          if (err) throw err;
          if (result) {
            result.forEach(item => {
              item.type = "artist";
              item.tracks = [];
              if (filter) {
                item.albums = item.albums.filter(album => { return filter.indexOf(album.visibility) > -1 });
              }
            });
          }
          process.stdout.write("services/db_manager ARTIST by id MONGO: " + id + "\n");
          callback(result[0]);
          redis.set(redis_key, result[0]);
        });
    }
  });
};

exports.filter = function (term, callback) {
  let aggregate = [
    { $project: { 'covers.cover64': false, 'covers.cover512': false } },
    { $match: { name: { $regex: term, $options: "i" } }, },
    { $limit: 6 }
  ]
  dbo
    .collection("artists")
    .aggregate(aggregate)
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.tracks = function (id, showPath, callback) {
  let request = [
    artist_lookup_album,
    {
      $unwind: { path: "$albums" }
    },
    artist_lookup_tracks,
    { $match: { _id: ObjectId(id) } },
    {
      $group: {
        _id: "$_id",
        name: { $first: "$name" },
        albums: { $push: "$albums" }
      }
    }
  ];

  if (!showPath) {
    request.push({
      $project: {
        "albums.tracks.path": false,
        "albums.tracks.bitrate": false,
        "albums.tracks.album_id": false,
        "albums.tracks.mime": false,
        "albums.artist_id": false,
        "albums.covers": false,
      }
    });
  }

  dbo
    .collection("artists")
    .aggregate(request)
    .toArray((err, result) => {
      if (result) {
        callback(result[0]);
      } else {
        callback(null);
      }
    });
};

exports.delete = function (artist, callback) {
  dbo.collection("artists")
    .deleteOne({ _id: ObjectId(artist._id) }, err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    });
};

exports.updateCovers = function (artist, covers, callback) {
  dbo.collection("artists").updateOne(
    { _id: ObjectId(artist._id) },
    { $set: { covers: covers } },
    { upsert: false },
    err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    }
  );
};

exports.empty = function (callback) {
  dbo
    .collection("artists")
    .aggregate([
      {
        $lookup: {
          from: "albums",
          localField: "_id",
          foreignField: "artist_id",
          as: "albums"
        }
      }
    ])
    .toArray((err, result) => {
      callback(result.filter(f => !f.albums || f.albums.length == 0));
    });
};