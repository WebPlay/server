const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
  dbo.collection("favourites").createIndex({ userId: 1 });
});

exports.collection = function (callback) {
  dbo
    .collection("users")
    .find({}, { password: false })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.favourites = function (id, callback) {
  dbo.collection("favourites")
    .find({ userId: id })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
}

exports.insertFavourite = function (item, callback) {
  item.itemId = ObjectId(item.itemId);
  dbo
    .collection("favourites")
    .insertOne(item, err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    });
}

exports.deleteFavourite = function (item, callback) {
  dbo
    .collection("favourites")
    .deleteMany({ userId: item.userId, itemId: ObjectId(item.itemId) }, (err) => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    });
}
