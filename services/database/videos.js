const { ObjectId } = require('mongodb');
var connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
  dbo.collection("videos").createIndex({ box_id: 1, title: 1 });
});

var server = require("../../server");
var config = server.config;

exports.exists = function (path, callback) {
  let short_path = path.replace(config.video_folder, "");
  dbo.collection("videos").findOne({ path: short_path }, (err, result) => {
    if (err) throw err;
    callback(result);
  });
};

exports.collection = function (callback) {
  dbo
    .collection("videos")
    .find()
    .toArray((err, result) => {
      result.forEach(item => {
        item.thumbnail = "";
      });
      callback(result);
    });
};

exports.byId = function (id, callback) {
  dbo
    .collection("videos")
    .findOne({ _id: ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.delete = function (video, callback) {
  dbo.collection("videos")
    .deleteOne({ _id: ObjectId(video._id) }, err => {
      if (err) throw err;
      callback();
    });
};

exports.moveTo = function (video, callback) {
  dbo
    .collection("videos")
    .updateOne(
      { _id: ObjectId(video._id) },
      { $set: { box_id: ObjectId(video.box_id) } },
      { upsert: false },
      err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      });
};

exports.mostViewed = function (filter, callback) {
  let aggregate = [{
    $group: {
      _id: "$id",
      title: { $last: "$title" },
      thumbnail: { $last: "$thumbnail" },
      parent: { $last: "$parent" },
      counter: { $sum: 1 },
      box: { $last: "$box" }
    },
  }]
  if (filter) {
    aggregate
      .unshift({
        $lookup: {
          from: "boxes",
          let: { box_id: "$parent._id" },
          pipeline: [{
            $match: {
              $expr: {
                $eq: ["$_id", { $toObjectId: "$$box_id" }]
              }
            }
          }
          ],
          as: "box"
        }
      }, { $unwind: "$box" })
    aggregate.push({
      $project: {
        "box.cover256": false,
        "box.cover128": false,
        "box.cover64": false,
        "box.cover32": false,
        "box.path": false
      }
    }, { $match: { "box.visibility": { $in: filter } } });
  }
  else {
    aggregate.unshift({ $match: { type: 'video' } });
  }
  aggregate.push(
    { $sort: { counter: -1, _id: -1 } },
    { $limit: 12 });

  dbo
    .collection("history")
    .aggregate(aggregate, {
      allowDiskUse: true
    })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};