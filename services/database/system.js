const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
});

exports.domains = function (callback) {
  dbo
    .collection("system")
    .findOne({ key: 'domains' })
    .then(result => {
      if (result) {
        callback(result.value);
      } else {
        callback([]);
      }
    });
};

exports.allows = function (callback) {
  dbo.collection("system")
    .findOne({ key: 'allows' })
    .then(allows => {
      if (!allows) {
        callback({
          "guests": false,
          "register": false,
        });
      } else {
        callback(allows.value);
      }
    })
}

exports.setAllows = function (allows, callback) {
  dbo.collection("system")
    .updateOne(
      { key: "allows" },
      { $set: { value: allows } },
      { upsert: true }, err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      });
}

exports.setDomains = function (domains, callback) {
  dbo.collection("system")
    .updateOne(
      { key: "domains" },
      { $set: { value: domains } },
      { upsert: true }, err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      });
}