const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
});

exports.get = function (parentId, callback) {
  dbo.collection("progress")
    .findOne({ parentId: parentId }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
}
exports.update = function (item) {
  dbo.collection("progress").deleteMany({ userId: ObjectId(item.userId), parentId: item.parentId }, () => {
    dbo.collection("progress").insertOne(item, (err) => {
      if (err) throw err;
    });
  });
};

exports.delete = function (item) {
  dbo.collection("progress").deleteMany({ userId: ObjectId(item.userId), parentId: item.parentId }, (err) => {
    if (err) throw err;
  });
}