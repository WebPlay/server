const redis = require("../redis")
const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
  dbo.collection("boxes").createIndex({ title: 1 });
  // TEMPORARY
  dbo.collection("boxes").updateMany({}, { $unset: { cover32: 1, cover64: 1, cover128: 1, cover256: 1, cover512: 1 } })
});

let box_project = {
  $project: {
    "videos.path": false,
    "videos.box_id": false,
    "videos.mime": false,
    path: false,
    "covers.cover32": false,
  }
}
let boxes_project = {
  $project: {
    path: false,
    "covers.cover32": false,
    "covers.cover64": false,
    "covers.cover256": false
  }
}
let box_lookup_videos = {
  $lookup: {
    from: "videos",
    localField: "_id",
    foreignField: "box_id",
    as: "videos"
  }
}

exports.collection = function (page, filter, callback) {
  process.stdout.write("services/db_manager BOXES Collection: " + page + "\n");
  let redis_key = "boxesCollection_" + (filter || '') + '_' + page;

  redis.get(redis_key, (value) => {
    if (value) {
      process.stdout.write("services/db_manager BOXES Collection REDIS: " + page + "\n");
      callback(value);
    } else {

      let aggregate = [
        boxes_project,
        { $match: { visibility: { $not: { $eq: 'hidden' } } } },
        { $sort: { title: 1 } },
      ];
      if (filter) {
        aggregate.push({
          $match: { visibility: { $in: filter } }
        });
      }
      if (page > -1) {
        let pageSize = 12;
        let skip = (page - 1) * pageSize;
        aggregate.push(
          { $skip: skip },
          { $limit: pageSize });
      }
      dbo
        .collection("boxes")
        .aggregate(aggregate, {
          allowDiskUse: true
        })
        .toArray((err, result) => {
          if (err) throw err;
          if (result) {
            result.forEach(item => {
              item.type = "box";
              item.videos = [];
            });
          }
          process.stdout.write("services/db_manager BOXES Collection MONGO: " + page + "\n");
          callback(result);
          redis.set(redis_key, result);
        });
    }
  });
};

exports.favourites = function (id, callback) {
  dbo.collection("favourites")
    .find({ userId: id, type: "box" })
    .toArray((err, favourites) => {
      if (err) throw err;
      let aggregate = [
        { $match: { _id: { $in: favourites.map(m => m.itemId) } } },
        boxes_project,
      ]
      dbo.collection("boxes")
        .aggregate(aggregate)
        .toArray((err, result) => {
          result.forEach(item => {
            item.type = "box";
            item.videos = [];
          });
          callback(result);
        });
    })
};


exports.newest = function (count, filter, callback) {
  let aggregate = [
    boxes_project,
    { $sort: { _id: -1 } },
    { $match: { visibility: { $not: { $eq: 'hidden' } } } }
  ];
  if (filter) {
    aggregate.push({
      $match: { visibility: { $in: filter } }
    });
  }
  aggregate.push({ $limit: count })

  dbo
    .collection("boxes")
    .aggregate(aggregate, {
      allowDiskUse: true
    })
    .toArray((err, result) => {
      if (err) throw err;
      if (result) {
        result.forEach(item => {
          item.type = "box";
          item.videos = [];
        });
      }
      callback(result);
    });
};


exports.byId = function (id, filter, callback) {
  process.stdout.write("services/db_manager BOX by id: " + id + "\n");
  let redis_key = "boxId_" + (filter || '') + '_' + id;

  redis.get(redis_key, (value) => {
    if (value) {
      process.stdout.write("services/db_manager BOX by id REDIS: " + id + "\n");
      callback(value);
    } else {
      let aggregate = [
        box_lookup_videos,
        box_project,
        { $match: { _id: ObjectId(id) } }
      ];
      if (filter) {
        aggregate.push({
          $match: { visibility: { $in: filter } }
        });
      }

      dbo
        .collection("boxes")
        .aggregate(aggregate)
        .toArray((err, result) => {
          if (err) throw err;
          if (result) {
            result.forEach(item => {
              item.type = "box";
            });
          }
          process.stdout.write("services/db_manager BOX by id MONGO: " + id + "\n");
          callback(result[0]);
          redis.set(redis_key, result[0]);
        });
    }
  });
};

exports.filter = function (term, callback) {
  let aggregate = [
    { $project: { 'covers.cover256': false } },
    { $match: { title: { $regex: term, $options: "i" } }, },
    { $limit: 6 }
  ]
  dbo
    .collection("boxes")
    .aggregate(aggregate)
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.empty = function (callback) {
  dbo
    .collection("boxes")
    .aggregate([box_lookup_videos])
    .toArray((err, result) => {
      callback(result.filter(f => !f.videos || f.videos.length == 0));
    });
};

exports.videos = function (id, showPath, callback) {
  let request = [
    {
      $lookup: {
        from: "videos",
        localField: "_id",
        foreignField: "box_id",
        as: "videos"
      }
    },
    { $match: { _id: ObjectId(id) } }
  ];

  if (!showPath) {
    request.push({
      $project: {
        "videos.mime": false,
        "videos.path": false,
        "videos.box_id": false,
        path: false,
        "covers.cover32": false,
        "covers.cover64": false,
        "covers.cover128": false
      }
    });
  }

  dbo
    .collection("boxes")
    .aggregate(request)
    .toArray((err, result) => {
      callback(result[0]);
    });
};

exports.delete = function (box, callback) {
  dbo.collection("boxes").deleteOne({ _id: box._id }, err => {
    if (err) throw err;
    callback();
  });
};

exports.update = function (box, callback) {
  dbo.collection("boxes").updateOne(
    { _id: ObjectId(box._id) },
    {
      $set: {
        visibility: box.visibility
      }
    },
    { upsert: false },
    err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    }
  );
}

exports.updateCovers = function (box, covers, callback) {
  dbo.collection("boxes").updateOne(
    { _id: ObjectId(box._id) },
    { $set: { covers: covers } },
    { upsert: false },
    err => {
      if (err) throw err;
      if (callback) {
        callback();
      }
    }
  );
};