const { ObjectId } = require('mongodb');
const connector = require("./CONNECTOR");
var dbo;
connector.connect().then((ret) => {
  dbo = ret;
  dbo.collection("tracks").createIndex({ path: 1, album_id: 1, "disk.no": 1, "track.no": 1 });
});

const server = require("../../server");
const config = server.config;

exports.collection = function (callback) {
  dbo
    .collection("tracks")
    .find()
    .toArray((err, result) => {
      callback(result);
    });
};

exports.byId = function (id, callback) {
  dbo
    .collection("tracks")
    .findOne({ _id: ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.delete = function (track, callback) {
  dbo.collection("tracks").deleteOne(track, err => {
    if (err) throw err;
    callback();
  });
};

exports.exists = function (path, callback) {
  dbo
    .collection("tracks")
    .find({ path: path.replace(config.music_folder, "") })
    .limit(1)
    .toArray((err, result) => {
      callback(result.length > 0);
    });
};

exports.moveTo = function (track, callback) {
  dbo
    .collection("tracks")
    .updateOne(
      { _id: ObjectId(track._id) },
      { $set: { album_id: ObjectId(track.album_id) } },
      { upsert: false },
      err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      });
};

exports.mostListened = function (filter, callback) {
  let aggregate = [{
    $group: {
      _id: "$id",
      title: { $last: "$title" },
      covers: { $last: "$covers" },
      parent: { $last: "$parent" },
      counter: { $sum: 1 },
      album: { $last: "$album" }
    },
  }]
  if (filter) {
    aggregate
      .unshift({
        $lookup: {
          from: "albums",
          let: { album_id: "$parent._id" },
          pipeline: [{
            $match: {
              $expr: {
                $eq: ["$_id", { $toObjectId: "$$album_id" }]
              }
            }
          }
          ],
          as: "album"
        }
      }, { $unwind: "$album" })
    aggregate
      .push({
        $project: {
          "album.cover256": false,
          "album.cover128": false,
          "album.cover64": false,
          "album.cover32": false
        }
      }, { $match: { "album.visibility": { $in: filter } } });
  } else {
    aggregate.unshift({ $match: { type: 'track' } });
  }
  aggregate.push({ $sort: { counter: -1, _id: -1 } }, { $limit: 6 })

  dbo
    .collection("history")
    .aggregate(aggregate, {
      allowDiskUse: true
    })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};